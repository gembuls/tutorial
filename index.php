<?php

function addOneToArray($array)
{
	foreach($array as $k => $v)
	{
		$array[$k] = $v+1;
	}
	
	return $array;
}

function addTreeToArray($array)
{
	foreach($array as $k => $v)
	{
		$array[$k] = $v+3;
	}
	
	return $array;
}

function addTwoToArray($array)
{
	foreach($array as $k => $v)
	{
		$array[$k] = $v+2;
	}
	
	return $array;
}


$array = [3,2,4,6,7,8,9];
$newarray =addOneToArray($array);

echo "<pre>";
var_dump($newarray);

$newarray =addTreeToArray($array);
var_dump($newarray);

$newarray =addTwoToArray($array);
var_dump($newarray);

?>